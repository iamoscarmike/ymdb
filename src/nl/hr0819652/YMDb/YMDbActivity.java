package nl.hr0819652.YMDb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class YMDbActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void OnBtnScanClick(View view){
    	Log.i("thomas", "OnBtnScanClick");
    	IntentIntegrator integrator = new IntentIntegrator(this);
    	integrator.initiateScan();
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    	  if (scanResult != null) {
    		  // handle scan result
    		  String barcode = scanResult.getContents();
    		  String type = scanResult.getFormatName();
    		  
    		  EditText etBarcode = (EditText) findViewById(R.id.etBarcode);
    		  EditText etType = (EditText) findViewById(R.id.etType);
    		  
    		  etBarcode.setText(barcode);
    		  etType.setText(type);
    	  }
    	  // else continue with any other code you need in the method
    	}
}